---
title: keys.openpgp.org Enhancement Proposal (KEP) Outline

---

This is a brainstorming document about enhancement proposals for `keys.openpgp.org`.

We're temporarily calling such an enhancement proposal a "KEP".
This document suggests parts that probably belong in every KEP.
It also suggests things that the KOO Board will probably want to to consider when reviewing a KEP.

Any KEP may want to include other information, of course, but the goal of this document is to describe pieces that any KEP author needs to consider.

# Problem statement

A clear, concise description of the problem that the KEP attempts to resolve.
This doesn't need a huge amount of detail, but it should motivate the rest of the proposal.

The proposal does not need to solve the problem entirely, but it should be clear how it contributes to a solution.

## Alternatives considered

The KEP author should outline any plausible alternatives that they have considered that would address the problem statement.
These considerations do not need to be in detail, and could simply be pointers to other projects.

The KEP author may want to clarify why the mechanism proposed in the KEP is preferable to each of the other alternatives.

# Specific work needed

This section outlines several different types of work that are likely to be necessary for an enhancement to be fruitful.

A KEP should identify the work items needed to succeed.

## Functional changes

What would change in the hagrid codebase?

This doesn't need to be a specific MR while the proposal is still in the KEP phase, but a sketch of the sort of things that would need to change is pretty important.

The board needs to be able to determine that it's plausible.

Some basic questions:

- how does the keystore evolve on disk due to these changes?
- how could any particular certificate mutate in light of these changes?
- how does the network API change?
- how does the website change (when driving from a browser)?
- how does the feature interact with other existing features?

## Functional tests

What kinds of tests are necessary to ensure that these changes to the codebase work correctly?

The board should identify specific tests that seem necessary to ensure that the feature is well-implemented and not buggy.

The KOO maintainers' opinion should have a lot of weight here, as they have operational experience.
If a KOO maintainer says that the proposed tests seem inadequate, that's a big warning flag.

## Transition planning

What kind of transition is necessary for the service once the code is merged?

For example, do we need a pass over the existing dataset before the feature goes live?
Can the transition be done in the background or does it require downtime?
How will the transition be tested before deployment?

KOO maintainers' opinion should have a lot of weight here as well.

## Outreach

What kind of interaction with the community is necessary for this feature to be useful?

Some features won't be useful unless the end user is made aware of them and opts in.
Others won't be useful unless there is advocacy among software developers.
Some features don't need any outreach at all, they can happen silently.

What kinds of outreach are needed to make this feature useful, and who needs to be targeted by this outreach?
How will they be reached?
Should the FAQ or any other static documentation need to be updated?

The board doesn't need to demand a detailed advertising budget here, but it should be wary of a KEP with no consideration of outreach needed.

## Utility tests

Some time after deployment, we'd like to ensure that the enhancement is useful on the live network service.

This is a sort of "key performance indicators" question.
For example, some ways it could be measured, depending on the enhancement:

- number of re-distributable certificates making use of the feature
- number of e-mail feedback loops that make use of the feature
- number of network requests using a specific API endpoint per week
- number of OpenPGP implementations exposing a particular feature in their API

But some utility tests may be particularly subtle and specific to the proposed enhancement.

The board should ensure that any proposed utility test includes a clearly-identified, quantifiable target, as well as a way to measure that target, and a plan to do so.

# Who will make those changes?

All of the above points describe concrete work to be done for the change to take effect.
Who is committing to do that work?
It does not need to be a single person for all of the changes.

The board needs to ensure that the identified workers all agree to their role in the KEP (easy to do if they are the author of the KEP).
The board also needs to determine whether it is plausible that the proposed workers could meet the deployment deadline.

# Known tradeoffs

This section should outline potential costs, risks, and drawbacks, should the KEP be adopted.
The subsections below identify some specific concerns, but the KEP author should also identify any other tradeoffs they can forsee.

When evaluating the KEP, each board member should also attempt to identify

## How does the change interact with the stated privacy policy?

Review [the privacy policy](https://keys.openpgp.org/about/privacy) and explain whether the change is conformant to it.

If it does not appear to be conformant, identify what would need to change (either in the proposal or the policy) for them to be aligned.

Changing the policy is a *very* complex operation, because KOO has interacted with hundreds of thousands of users who may have based their decision on the current privacy policy.

Any KEP that involves changing the privacy policy needs a clear story about:

- how the change in privacy policy will be communicated to users
- how KOO will distinguish between data handled only under the old privacy policy, data handled under the new privacy policy, and data that might be mixed.

It is better to make sure the KEP aligns with the current privacy policy.

## What additional resource consumption costs do we expect from the change?

At the very least:

- bandwidth
- storage
- compute

Are there other resources the board should consider?
These are understood to be estimates, but they should be quantified, and the reasoning should be explained.

## What changes for the user stories?

`keys.openpgp.org` has a handful of canonical user stories (do we have a reference to these?).
For example:

- Someone with an e-mail address wants to publish their OpenPGP certificate for the world to find.
- Someone sending e-mail wants to find an OpenPGP certificate to use to encrypt their e-mail.
- Someone verifying an OpenPGP signature wants to learn the e-mail address of the signer.
- A domain operator wants to delegate WKD for their domain to `keys.openpgp.org`.

Each of these user stories has a particular workflow for the current operation of the service.
Does the workflow for any of these stories change as the result of the KEP?
Are there new user stories that become relevant after the adoption of the KEP?

## What additional duties does this KEP impose on the operations team?

The operations team is responsible for keeping the service active and functional.
Some KEPs may require additional ongoing work for the operations team.
For example, a KEP may create a queue of some sort that the operations team needs to monitor, and take action if it grows too large.
Or, a KEP may create a new ongoing administrative task (such as annual DNS registration renewal).

A reasonable KEP will be aware of these additional duties, and outline them as clearly as possible.

The board should ensure that the operations team seems confident that the KEP is realistic about the additional duties it might impose, and that those duties seem achievable.

In the event that additional ongoing duties are required that need more resources than are currently available to the operations team, the KEP should propose how to make the process more sustainable.

# Deadlines

## Deployment Deadline

The deployment deadline is the date by which if the changes are not ready to be deployed (e.g. feature not complete, tests missing or failing, transition code unverified), the KEP becomes "abandoned" (what happens to an "abandoned" KEP?).

If everything is ready before the deployment deadline, deployment can proceed ahead of deadline.

## Evaluation Deadline

The evaluation deadline is specified as a duration that starts at deployment.
It should not be longer than two years, and probably shouldn't be shorter than a month.

Features that might legitimately want a longer evaluation deadline would include things that require significant work on the rest of the ecosystem.
(e.g. convincing stable OpenPGP implementations to query a new API endpoint, or to produce new OpenPGP material)

Features that might legitimately want a shorter evaluation deadline are things that adjust live parameters of the operating service.

The board should consider whether the evaluation deadline seems reasonable for the proposed change.

When the evaluation deadline expires, the utility tests should be evaluated, to see whether the goals have been met.

# Rollback plans

If the utility test goals have not been met after the evaluation deadline, the KOO maintainers may decide to roll these changes back.

What steps would be necessary to roll the changes back?

What would the impact of the rollback be?
What would the impact of not rolling back the enhancement be, if the KEP fails to meet its utility test goals?

The board should evaluate the plausibility of the rollback plans, and consider the impact on the ecosystem of such a change.
