# Electoral Process for KOO

This document describes process and timing for all elections that happen among the KOO voting body.
Its purpose is to provide continuity, transparency, and accountability about electoral process to the broader KOO community.

The Board may change this document by usual Board action.

In the event that this document conflicts with the constitution, the constitution takes precedence.

There are three kinds of possible KOO voting elections that require participation of the KOO voting body:

- Electing the board
- Removng a member from the voting body
- Amending the constitution

## Shared logistical details

All KOO elections share some standard logistics.

### The election organizer role

Every election has a designated organizer, typically the secretary of the Board.

The designated organizer may explicitly delegate the election organizer responsibility to any other willing member of the voting body.

### The baseline election announcement

Regardless of the type of election, each election begins with a formal election announcement from the election's organizer.

All election announcements contain this baseline:

- The unique name of the election (e.g. "KOO Board election Summer 2022")
- How the ballot shall be structured
- How the ballot shall be transmitted and received
- The list of eligible voters, identified by OpenPGP certificate and voting body member
- The time that voting begins
- The time that voting ends

Each type of election requires some additional information, as noted below.

### Voting body membership changes during an election

Membership in the KOO voting body can change at any time, either by adding new members or removing existing members.

However, the relevant voting body membership for a given election is fixed at the time when the election is announced.

A voting member added after an election announcement may not cast a ballot in that election.
A voting member removed after an election announcement may cast a ballot in that election.

For those elections that require a quorum, if the voting body changes after the election announcement, the quorum is calculated from that election's list of voters, not by the current voting body membership.

### Unreadable ballots

A ballot may be received which cannot be unambiguously interpreted by the election organizer's stated explanation about how the ballot should be structured.
Such a ballot is considered invalid.
No part of an invalid ballot is considered for the vote at all, even if there is some unambiguous material in it.

### Voting confirmation

For all elections, the election organizer should publicly acknowledge each ballot received within the eligible voting period within 48 hours of receipt.
The acknowledgement should indicate:

- who cast the ballot
- whether the ballot is considered readable

### Followup ballots (changing a ballot during voting period)

When a candidate casts a ballot (or a commitment to a ballot, in a remote election where the ties will be broken by data from the election), it invalidates any previous ballot cast by the same voter in the same election.

When adjudicating which ballot from a voter supersedes which other one, the latest OpenPGP timestamp wins.

If two different ballots from the same voter for the same election with the same OpenPGP timestamp exist, and no ballot received from that voter for that election has a more recent OpenPGP timestamp, both ballots are invalidated.

## Electing the Board

Board elections have the most complex process.

A Board election process begins when the election organizer clearly announces the election.
In addition to the baseline election announcement, the following additional information is present:

- How a member of the voting body announces their intent to stand as a candidate
- What tie-breaking mechanism will be used, in specific detail (see tie-breaking.md)

Note that the list of eligible voters in the announcement is also the same as the list of potential candidates.

When the election is announced, any member of the voting body may formally self-nominate for candidacy until the time that the voting begins.

There must be at least two weeks between the announcement of the election and the time the voting begins to allow time for candidates to self-nominate.

The voting end time is two weeks after the voting start time.

When the voting begins, the election organizer sends out a secondary announcement, containing the same info as the first announcement, plus:

- The list of self-nominated candidates

When the voting period ends, the election organizer sends an final announcement indicating that the voting period is over.

If the voting is using commitment-based tie-breaking (see tie-breaking.md), voters who submitted only a commitment have 24 hours to submit a ballot that matches their commitment.

When all ballots have been received, the election organizer has 24 hours to produce the preliminary results.
The preliminary results are:

- a list of self-nominated candidates who received at least five approvals
- each listed candidate has an indication of how many approvals they received
- the list is ordered by the number of approvals, with ties already broken according to the approach specified in the first announcement

Each approved candidate has an opportunity to publicly accept or decline being seated.
They are expected to do so promptly, within 24 hours of being informed of the answer of all the candidates above them in the list.
An approved candidate need not wait for the response of any higher-ranked candidate to publish their decision.
If an approved candidate does not publicly accept or decline within 24 hours of the latest public response of all candidates ranked above them, they are considered to have declined automatically.

The election organizer announces the membership of the new Board as soon as it is determined from the public responses.

If an existing Board is active, the new Board assumes control 1 year after the start of the previous Board's term.
If no existing Board is active, the new Board's term begins immediately.

### Overall timing for a Board election

The Board election process takes place over about 5 weeks after the announcement:

- ~2 weeks for candidacy self-nominations
- ~2 weeks for voting and commitments
- ~1 week for final balloting and accepting/declining

The organizer for a regularly-scheduled Board election should start the process no sooner than 10 months after the Board assumed control.
This minimizes the "lame duck" period for the outgoing board while leaving room for delays in the accept/decline process.

Ideally, the new Board will be known before the old Board's term ends, so that the transition process between Boards (sensitive information handover, KEP adoption transfer, etc) happens smoothly.

### Late nominations

If a candidate self-nominates after the voting begins, they are not eligible for the election no matter how many approvals they receive.

### Withdrawing from the election

If a self-nominated candidate withdraws before the voting starts, they will not be listed in the set of eligible candidates.

A formal withdrawal during the voting period means only that the candidate has committed to declining any Board seat that they might be offered.

## Removing a member from the voting body

When a Board member formally nominates one or more voting members for removal from the voting body, an election is triggered.

The election organizer announces the election within 1 week of the nomination.
In addition to the baseline election announcement, the following additional information is present:

- The list of voting members proposed for removal, and which Board member nominated each one

The voting must begin at least 1 week and no more than 2 weeks from the time of the announcement.
This time gives time for advocates for and opponents of removal to argue for their positions.
 
The voting end time is one week after the voting start time.

Each voter must be able to confirm, deny, or abstain from the removal of each proposed voting member.

There is no tie-breaking.
For each member proposed for removal, they are removed from the voting body if the number of confirmations exceeds the number of denials.

Once formally nominated for removal, the vote proceeds.
There is no way to rescind such a nomination.

### Overall timing for a membership removal vote

The process to remove membership from a member of the voting body takes place between 2 and 4 weeks:

- up to a week from nomination to announcement
- 1 to 2 weeks from announcement to voting start
- 1 week from voting start to voting end

## Constitutional amendment

A member of the voting body formally proposes a specific amendment to the constitution.
The proposed amendment should be in the form of a unified diff that applies only to the constitution.md file.

The amendment must be seconded within 30 days of being proposed.
Note that if the 30 days elapses without a second, the proposer may simply re-propose the same amendment if they think it has a chance of getting a second.
The goal of this first 30 days is just to avoid having an ever-growing queue of outstanding proposed amendments.

When there is a proposer and at least one seconder, and at least one of these people is a Board member, an election is triggered.

The election organizer announces the election within 1 week of the election being triggered.
In addition to the baseline election announcement, the following additional information is present:

- The proposed change to constitution.md, in unified diff form
- Who proposed and who seconded the amendment

The voting must begin at least 2 weeks and no more than 4 weeks from the time of the announcement.

The voting end time is two weeks after the voting start time.

Each voter must be able to approve or reject the amendment.
To abstain, a voting member should simply not cast a ballot.

There is no tie-breaking.

50% of the voting body must vote for the amendment to reach quorum.
At the end of the voting, if 67% or more of the ballots cast approve the amendment, then the proposed diff is merged into constitution.md.

### Overall timing for a Constitutional amendment vote

The process for a constitutional amendment takes from 4 to about 11 weeks:

- 0 to 30 days for proposing and seconding
- up to 1 week after seconding to announcement
- 2 to 4 weeks from the announcement to voting start
- 2 weeks from voting start to voting end

### Withdrawing a proposed amendment

If the proposer withdraws a proposed amendment before the election is announced, the process stops.

If any seconder withdraws their endorsement after the election is triggered, but before the election is announced, the election organizer is not obliged to announce the election unless at least the proposer and one other seconder remain, and one of them is on the Board.

Once the election is announced, the vote proceeds.
There is no way for the proposer or any seconder to rescind a proposal once the election is announced.
